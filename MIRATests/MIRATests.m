//
//  MIRATests.m
//  MIRATests
//
//  Created by Bob Howard on 7/11/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface MIRATests : XCTestCase

@end

@implementation MIRATests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
