//
//  AppDelegate.m
//  MIRA
//
//  Created by Bob Howard on 7/11/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "AppDelegate.h"

#import <Parse/Parse.h>
#import "MiraUser.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	// setup the connection to the Back4App Parse-based backend services
	[Parse initializeWithConfiguration:[ParseClientConfiguration configurationWithBlock:^(id<ParseMutableClientConfiguration>  _Nonnull configuration) {
		configuration.applicationId = @"SrrvxkcECSZBnJhyZCo8imUyo64uH82NROrdhvU8";
		configuration.clientKey = @"pvSFWEJLVCBDMeKRkF8YSSIzBEsBSYP4scoynB6z";
		configuration.server = @"https://parseapi.back4app.com";
	}]];
	
	[MiraUser logInWithUsernameInBackground:@"kalharri" password:@"password"
									block:^(PFUser *user, NSError *error)
	 								{
										if (user)
										{
											MiraUser *loggedOnUser = (MiraUser *)user;
											[loggedOnUser.profession fetchIfNeeded];
											[loggedOnUser.institution fetchIfNeeded];
											NSLog(@"***** LOGGED ON! %@ at %@", loggedOnUser.profession.name, loggedOnUser.institution.name);
										}
										else
										{
											NSLog(@"\\\\ COULDN't VERIFY USER!");
										}
									}
	 ];
	
	UIPageControl *pageControl = [UIPageControl appearance];
	pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
	pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
	pageControl.backgroundColor = [UIColor whiteColor];
	
	return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
	// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
