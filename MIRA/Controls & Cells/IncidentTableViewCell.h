//
//  IncidentTableViewCell.h
//  MIRA
//
//  Created by Bob Howard on 7/24/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IncidentTableViewCell : UITableViewCell

@end

NS_ASSUME_NONNULL_END
