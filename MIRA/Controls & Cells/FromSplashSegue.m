//
//  FromSplashSegue.m
//  MIRA
//
//  Created by Bob Howard on 8/6/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "FromSplashSegue.h"

@implementation FromSplashSegue

-(void) perform
{
	// get each end's controller's view
	UIView *oldView = ((UIViewController *)self.sourceViewController).view;
	UIView *newView = ((UIViewController *)self.destinationViewController).view;
	oldView.alpha = 1.0;
	newView.alpha = 0.0;

	// get the main application window
	UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
	
	// position newView same as oldView and place it above oldView
	newView.center = oldView.center;
	[window insertSubview:newView aboveSubview:oldView];

	// do the visual transition
	[UIView animateWithDuration:0.6
					 animations:^{
						 newView.alpha = 1.0;
						 oldView.alpha = 0.0;
					 			 }
					 completion:^(BOOL finished){
						 [oldView removeFromSuperview];
						 
						 // finally, transition the behavior
						 window.rootViewController = self.destinationViewController;
					 }];

	
	
	
/*  	// this does a push left
 
 // get each end's controller's view
 UIView *oldView = ((UIViewController *)self.sourceViewController).view;
 UIView *newView = ((UIViewController *)self.destinationViewController).view;
 
 // get the main application window
 UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
 
 // position newView to the right of oldView and place it above oldView
 newView.center = CGPointMake(oldView.center.x + oldView.frame.size.width, newView.center.y);
 [window insertSubview:newView aboveSubview:oldView];
 
 // do the visual transition

	[UIView animateWithDuration:0.4
					 animations:^{
						 newView.center = CGPointMake(oldView.center.x, newView.center.y);
						 oldView.center = CGPointMake(0 - oldView.center.x, newView.center.y);}
					 completion:^(BOOL finished){
						 [oldView removeFromSuperview];
						 
						 // finally, transition the behavior
						 window.rootViewController = self.destinationViewController;
					 }];
	
*/
}

@end
