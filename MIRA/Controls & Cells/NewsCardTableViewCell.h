//
//  NewsCardTableViewCell.h
//  MIRA
//
//  Created by Bob Howard on 7/14/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/PFImageView.h>
#import "MeraNewsArticle.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewsCardTableViewCell : UITableViewCell

@property (strong, nonatomic) MeraNewsArticle *article;		// ParseObject analog contains news article metadata

@end

NS_ASSUME_NONNULL_END
