//
//  CurveAnimation.m
//  MIRA
//
//  Created by Bob Howard on 8/25/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//


#import "CurveAnimation.h"

@interface CurveAnimation()

@property (strong, nonatomic)	UIView			*loaderView;
@property (strong, nonatomic)	CADisplayLink	*displayLink;
@property (strong, nonatomic)	UITextView		*loadingText;

@property (weak, nonatomic)		UIView			*rootView;
@property (strong, nonatomic)	NSMutableArray 	*aShapes;

@end

@implementation CurveAnimation

// NSMutableArray 	*aShapes;
CGRect			bounds;


- (CurveAnimation *) initWithView:(UIView *)newView
{
	self.rootView = newView;
	bounds = CGRectInset(self.rootView.bounds, 12, 12);
	self.displayLink = [CADisplayLink displayLinkWithTarget:self selector:NSSelectorFromString(@"animate")];
	[self.displayLink addToRunLoop:NSRunLoop.currentRunLoop forMode:NSDefaultRunLoopMode];
	
	self.loaderView = [[UIView alloc] initWithFrame:bounds];
	self.loaderView.backgroundColor = [UIColor clearColor];

	self.loadingText = [[UITextView alloc] init];
	self.loadingText.text = @"Creating account...";
	self.loadingText.font = [UIFont fontWithName:@"Avenir" size:28];
	self.loadingText.textColor = [UIColor darkGrayColor];
	self.loadingText.backgroundColor = [UIColor clearColor];
	[self.loadingText sizeToFit];
	self.loadingText.center = CGPointMake(bounds.size.width / 2.0, bounds.size.height / 1.1);
	
	self.aShapes = [NSMutableArray array];

	for (int n = 0; n < 3; n++)
	{
		[self.aShapes addObject:[self shapesSetup]];
	}
	
	return self;
}


-(void) setup
{
	NSArray *colors = @[
						[UIColor orangeColor],
						[UIColor purpleColor],
						[UIColor greenColor]
					   ];
	
	for (int n = 0; n < 3; n++)
	{
		((CAShapeLayer *)self.aShapes[n]).fillColor = [colors[n] colorWithAlphaComponent:0.7].CGColor;
		[self.loaderView.layer addSublayer:self.aShapes[n]];
	}

	[self.loaderView addSubview:self.loadingText];
	[self.rootView addSubview:self.loaderView];
	
	[self textAnimationSetup:self.loadingText];
	
	self.displayLink.paused = false;
	self.displayLink.preferredFramesPerSecond = 5;
}

-(void) remove

{
	[UIView animateWithDuration:1
					 animations:^{
						 self.loaderView.alpha = 0.0;
					 }
					 completion:^(BOOL finished) {
						 [self.loaderView.layer removeAllAnimations];
						 [self.loaderView removeFromSuperview];
					 }];
}

#pragma mark - helper NSMethods

- (CAShapeLayer *)shapesSetup
{
	CAShapeLayer *shape = [CAShapeLayer new];
	shape.path = [self randPath].CGPath;
	shape.strokeColor = [UIColor clearColor].CGColor;
	shape.lineWidth = 3;
	
	return shape;
}

- (void) animationSetup:(CAShapeLayer *)layer
{
	CABasicAnimation *animate = [CABasicAnimation animationWithKeyPath:@"path"];
	
	animate.toValue = (__bridge id _Nullable)([self randPath].CGPath);
	animate.duration = 1.7;
	animate.fillMode = kCAFillModeForwards;
	animate.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];

	[layer addAnimation:animate forKey:nil];
}

- (void) textAnimationSetup:(UITextView *)textView
{
	CABasicAnimation *animate = [CABasicAnimation animationWithKeyPath:@"opacity"];
	
	animate.fromValue = [NSNumber numberWithFloat:1.0];
	animate.toValue   = [NSNumber numberWithFloat:0.5];

	animate.duration = 1.0;
	animate.fillMode = kCAFillModeForwards;
	animate.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
	animate.autoreverses = true;
	animate.repeatCount = 8000.0;
	
	[textView.layer addAnimation:animate forKey:nil];
}

- (UIBezierPath *) randPath
{
	UIBezierPath *path = [UIBezierPath bezierPath];
	
	CGPoint endPoint = CGPointMake(bounds.size.width - 10.0, bounds.size.height / 2.0 - 160.0);			// 300.0);
	[path moveToPoint:CGPointMake(10.0, bounds.size.height / 2.0)];
	
	CGFloat randY = 220.0 + drand48() * 320.0;
	
	CGPoint c1 = CGPointMake(150.0, randY + 160.0);
	CGPoint c2 = CGPointMake(250.0, randY - 160.0);
	
	[path addCurveToPoint:endPoint controlPoint1:c1 controlPoint2:c2];
	
	return path;
}

- (void) animate
{
	for (CAShapeLayer *sLayer in self.aShapes)
	{
		[self animationSetup:sLayer];
	}
}


@end
