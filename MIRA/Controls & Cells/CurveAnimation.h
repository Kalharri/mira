//
//  CurveAnimation.h
//  MIRA
//
//  Created by Bob Howard on 8/25/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CurveAnimation : NSObject


- (CurveAnimation *) initWithView:(UIView *)newView;


- (void) setup;
- (void) remove;

@end

NS_ASSUME_NONNULL_END
