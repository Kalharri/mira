//
//  FromSplashSegue.h
//  MIRA
//
//  Created by Bob Howard on 8/6/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FromSplashSegue : UIStoryboardSegue

@end

NS_ASSUME_NONNULL_END
