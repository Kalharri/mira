//
//  NewsCardTableViewCell.m
//  MIRA
//
//  Created by Bob Howard on 7/14/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "NewsCardTableViewCell.h"

@interface NewsCardTableViewCell ()

@property (weak, nonatomic) IBOutlet UIView 		*whiteCardView;		// card view holds the others, makes white bkgnd & shadow
@property (weak, nonatomic) IBOutlet UILabel		*articleDate;		//
@property (weak, nonatomic) IBOutlet UILabel		*articleTitle;		//
@property (weak, nonatomic) IBOutlet PFImageView	*articleThumbnail;	// image to display with article card
@end

@implementation NewsCardTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];			// ***B	Is this really necessary?
}

- (void)layoutSubviews
{
	self.whiteCardView.alpha = 1.0;
	
	// allow the card views shadow to cover it's parent's surface
	self.whiteCardView.layer.masksToBounds = NO;
	
	// give the card its shadow
	self.whiteCardView.layer.shadowOffset = CGSizeMake(3.0, 3.0);
	self.whiteCardView.layer.shadowRadius = 6.0;
	self.whiteCardView.layer.shadowOpacity = 0.4;
	UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.whiteCardView.bounds];
	self.whiteCardView.layer.shadowPath = shadowPath.CGPath;
	
	if (self.article != nil)
	{
		// fill the subViews' content from our cached MeraNewsArticle object
//		NSLog(@"%@", self.article.articleTitle);
		self.articleTitle.text = self.article.articleTitle;
		
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		NSString *formatString = [NSDateFormatter dateFormatFromTemplate:@"dd MMM YYYY" options:0 locale:[NSLocale currentLocale]];
		[dateFormatter setDateFormat:formatString];
		self.articleDate.text = [dateFormatter stringFromDate:self.article.articleDate];

		self.articleThumbnail.file = self.article.thumbnail;
		[self.articleThumbnail loadInBackground];
	}

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
