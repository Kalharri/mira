//
//  ScaleButton.m
//  MIRA
//
//  Created by Bob Howard on 7/17/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "ScaleButton.h"
#import <QuartzCore/QuartzCore.h>

@interface ScaleButton ()


@end


@implementation ScaleButton


#pragma mark - lifecycle

+ (id)buttonWithType:(UIButtonType)type
{
	return [super buttonWithType:UIButtonTypeCustom];
}

- (id)init
{
	if (self = [super init])
	{
		[self commonInit];
	}
	return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
	if (self = [super initWithCoder:aDecoder])
	{
		[self commonInit];
	}
	return self;
}

-(id)initWithFrame:(CGRect)frame
{
	if (self = [super initWithFrame:frame])
	{
		[self commonInit];
	}
	return self;
}

- (void)setEnabled:(BOOL)enabled
{
	// ScaleButtons animate the visual enabling of their borders
	[super setEnabled:enabled];
	
	[UIView animateWithDuration:0.3f animations:^{
		
		if (enabled)
		{
			self.layer.borderColor = [UIColor colorNamed:@"miraAction"].CGColor;
		}
		else
		{
			self.layer.borderColor = [UIColor lightGrayColor].CGColor;
		}
	}];
	
	[self setNeedsDisplay];
}

- (void)setSelected:(BOOL)selected
{
	[super setSelected:selected];
	
	// ScaleButtons animate their visual selection, as well
	[UIView animateWithDuration:0.3f animations:^{
		
		if (selected)
		{
			self.backgroundColor = self.tintColor;
		}
		else
		{
			self.backgroundColor = [UIColor clearColor];
		}
	}];
	
	[self setNeedsDisplay];
}


#pragma mark - helper functions


-(void) commonInit
{
	self.autoresizesSubviews = TRUE;

	// make the buttons "pill" shaped
	self.layer.cornerRadius = 14.0;
	self.layer.masksToBounds = true;

	// if enabled, color the scaleButton's border with our actionable orange, otherwise the disabled gray
	self.layer.borderWidth = 1.5;
	self.layer.borderColor = self.enabled ? [UIColor colorNamed:@"miraAction"].CGColor : [UIColor lightGrayColor].CGColor;	
}



@end
