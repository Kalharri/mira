//
//  InstitutionClusterView.m
//  MIRA
//
//  Created by Bob Howard on 9/22/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "InstitutionClusterView.h"

@implementation InstitutionClusterView


- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    if ((self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier]))
	{
        self.displayPriority = MKFeatureDisplayPriorityDefaultHigh;
        self.collisionMode = MKAnnotationViewCollisionModeCircle;
    }

    return self;
}

- (void)setAnnotation:(id<MKAnnotation>)annotation
{
    super.annotation = annotation;
    [self updateImage:annotation];
}

- (void)updateImage:(MKClusterAnnotation *)cluster
{
    if (!cluster)
	{
        self.image = nil;
        return;
    }

    CGRect rect = CGRectMake(0, 0, 40, 40);
    UIGraphicsImageRenderer *renderer = [[UIGraphicsImageRenderer alloc] initWithSize:rect.size];
    self.image = [renderer imageWithActions:^(UIGraphicsImageRendererContext * _Nonnull rendererContext)
	{
        // circle

        [[UIColor blueColor] setFill];
        [[UIColor lightGrayColor] setStroke];

        UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:rect];
        path.lineWidth = 0.5;
        [path fill];
        [path stroke];

        // count

        NSString *text = [NSString stringWithFormat:@"%ld", (long) cluster.memberAnnotations.count];
        NSDictionary<NSAttributedStringKey, id> *attributes = @{
            NSFontAttributeName: [UIFont preferredFontForTextStyle: UIFontTextStyleBody],
            NSForegroundColorAttributeName: [UIColor whiteColor]
                                                                };
        CGSize size = [text sizeWithAttributes:attributes];
        CGRect textRect = CGRectMake(rect.origin.x + (rect.size.width  - size.width)  / 2,
                                     rect.origin.y + (rect.size.height - size.height) / 2,
                                     size.width,
                                     size.height);
        [text drawInRect:textRect withAttributes:attributes];
    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
