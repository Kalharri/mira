//
//  InstitutionAnnotationView.m
//  MIRA
//
//  Created by Bob Howard on 9/21/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "InstitutionAnnotationView.h"

static NSString *identifier = @"com.brainweasel.clusteringIdentifier";


@implementation InstitutionAnnotationView


- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    if ((self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier]))
	{
        self.clusteringIdentifier = identifier;
        self.collisionMode = MKAnnotationViewCollisionModeCircle;
		
		self.markerTintColor = [UIColor colorNamed:@"miraApp"];
		self.animatesWhenAdded = true;
		
    }

    return self;
}

- (void)setAnnotation:(id<MKAnnotation>)annotation
{
    [super setAnnotation:annotation];

    self.clusteringIdentifier = identifier;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
