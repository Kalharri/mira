//
//  ReportView.m
//  MIRA
//
//  Created by Bob Howard on 7/26/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "ReportView.h"

@interface ReportView ()

@property (weak, nonatomic) IBOutlet UIView *incidentListContainer;

@end

@implementation ReportView

- (void) sendListToBack:(BOOL) back
{
	if (back)
	{
		[self sendSubviewToBack:self.incidentListContainer];
	}
	else
	{
		[self bringSubviewToFront:self.incidentListContainer];
	}
}

@end
