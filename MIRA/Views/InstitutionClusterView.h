//
//  InstitutionClusterView.h
//  MIRA
//
//  Created by Bob Howard on 9/22/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <MapKit/MapKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InstitutionClusterView : MKAnnotationView

@end

NS_ASSUME_NONNULL_END
