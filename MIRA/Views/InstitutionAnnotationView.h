//
//  InstitutionAnnotationView.h
//  MIRA
//
//  Created by Bob Howard on 9/21/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <MapKit/MapKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InstitutionAnnotationView : MKMarkerAnnotationView

@end

NS_ASSUME_NONNULL_END
