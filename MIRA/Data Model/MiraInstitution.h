//
//  MiraInstitution.h
//  MIRA
//
//  Created by Bob Howard on 8/2/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <Parse/Parse.h>

NS_ASSUME_NONNULL_BEGIN

@interface MiraInstitution : PFObject  <PFSubclassing>

+ (NSString *)parseClassName;

@property (nonatomic, strong) NSNumber		*providerId;		// 6-digit, goverment issued
@property (nonatomic, strong) NSString		*name;					// facility name
@property (nonatomic, strong) NSString		*address;			// mailing address
@property (nonatomic, strong) NSString		*city;					// mailing address
@property (nonatomic, strong) NSString		*state;					// 2-digit state code
@property (nonatomic, strong) NSNumber		*zipcode;			// 6-digit version
@property (nonatomic, strong) PFGeoPoint	*location;			// copied to each report a user files, for efficient map display
@property (nonatomic, strong) NSString		*county;
@property (nonatomic, strong) NSString		*type;					// 
@property (nonatomic, strong) NSString		*ownership;		// voluntary|government, for|non profit, private|church, etc.

@property (nonatomic, strong) NSNumber		*weekTotal;		// totals computed by daily cloudCode job on server
@property (nonatomic, strong) NSNumber		*monthTotal;
@property (nonatomic, strong) NSNumber		*yearTotal;

@end

NS_ASSUME_NONNULL_END
