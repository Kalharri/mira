//
//  MiraUser.m
//  MIRA
//
//  Created by Bob Howard on 8/29/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "MiraUser.h"
#import <Parse/PFObject+Subclass.h>

@implementation MiraUser

@dynamic institution;
@dynamic profession;



+ (nonnull NSString *)parseClassName
{
	return [super parseClassName];


	// allows Parse engine object serialization
//	return NSStringFromClass([self class]);
}

@end
