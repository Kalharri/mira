//
//  MiraProfessionalRole.h
//  MIRA
//
//  Created by Bob Howard on 8/27/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <Parse/Parse.h>
#import "MiraProfessionalCategory.h"

NS_ASSUME_NONNULL_BEGIN

@interface MiraProfessionalRole : PFObject  <PFSubclassing>

+ (NSString *)parseClassName;

@property (nonatomic, strong) NSString					*roleId;
@property (nonatomic, strong) NSString					*name;
@property (nonatomic, strong) MiraProfessionalCategory	*category;

@end

NS_ASSUME_NONNULL_END
