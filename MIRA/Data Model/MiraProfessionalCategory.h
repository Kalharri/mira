//
//  MiraProfessionalCategory.h
//  MIRA
//
//  Created by Bob Howard on 8/27/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <Parse/Parse.h>

NS_ASSUME_NONNULL_BEGIN

@interface MiraProfessionalCategory : PFObject  <PFSubclassing>

+ (NSString *)parseClassName;

@property (nonatomic, strong) NSString	*name;

@end

NS_ASSUME_NONNULL_END
