//
//  MiraInstitution.m
//  MIRA
//
//  Created by Bob Howard on 8/2/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "MiraInstitution.h"
#import <Parse/PFObject+Subclass.h>

@implementation MiraInstitution

@dynamic providerId;
@dynamic name;
@dynamic address;
@dynamic city;
@dynamic state;
@dynamic zipcode;
@dynamic location;
@dynamic county;
@dynamic type;
@dynamic ownership;
@dynamic weekTotal;
@dynamic monthTotal;
@dynamic yearTotal;


+ (nonnull NSString *)parseClassName
{
	// allows Parse engine object serialization
	return NSStringFromClass([self class]);
}

@end
