//
//  NewsArticle.m
//  MIRA
//
//  Created by Bob Howard on 7/20/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "MeraNewsArticle.h"
#import <Parse/PFObject+Subclass.h>

@implementation MeraNewsArticle

@dynamic articleDate;
@dynamic articleTitle;
@dynamic thumbnail;
@dynamic bodyPDF;



+ (nonnull NSString *)parseClassName
{
	// allows Parse engine object serialization
	return NSStringFromClass([self class]);
}

@end
