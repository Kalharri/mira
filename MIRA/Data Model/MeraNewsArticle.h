//
//  MeraNewsArticle.h
//  MIRA
//
//  Created by Bob Howard on 7/20/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <Parse/Parse.h>

NS_ASSUME_NONNULL_BEGIN

@interface MeraNewsArticle : PFObject  <PFSubclassing>

+ (NSString *)parseClassName;

@property (nonatomic, strong) NSDate		*articleDate;		// published date for display on card
@property (nonatomic, strong) NSString		*articleTitle;		// article title for display
@property (nonatomic, strong) PFFileObject	*thumbnail;			// image on card
@property (nonatomic, strong) PFFileObject	*bodyPDF;			// contains the PDF data to view

@end

NS_ASSUME_NONNULL_END
