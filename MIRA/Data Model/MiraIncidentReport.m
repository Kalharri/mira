//
//  MMiraIncidentReport.m
//  MIRA
//
//  Created by Bob Howard on 8/2/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "MiraIncidentReport.h"
#import <Parse/PFObject+Subclass.h>

@implementation MiraIncidentReport

@dynamic reportingUser;
@dynamic institution;
@dynamic incidentType;
@dynamic patientHarm;
@dynamic prose;
@dynamic photo;
@dynamic reportingLocation;

+ (nonnull NSString *)parseClassName
{
	// allows Parse engine object serialization
	return NSStringFromClass([self class]);
}

@end
