//
//  MiraIncidentReport.h
//  MIRA
//
//  Created by Bob Howard on 8/2/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <Parse/Parse.h>
#import "MiraUser.h"
#import "MiraInstitution.h"

NS_ASSUME_NONNULL_BEGIN

@interface MiraIncidentReport : PFObject  <PFSubclassing>

+ (NSString *)parseClassName;

@property (nonatomic, strong) MiraUser					*reportingUser;				// pointer to PFUser object
@property (nonatomic, strong) MiraInstitution	*institution;					// pointer to MiraInstitution object
@property (nonatomic, strong) PFObject					*incidentType;				// pointer to MiraIncidentType object
@property (nonatomic, strong) NSNumber					*patientHarm;					// 0-5, 0 representing no harm and 5 representing death
@property (nonatomic, strong) NSString					*prose;									// user impressions
@property (nonatomic, strong) PFFileObject			*photo;									// documenting pic
@property (nonatomic, strong) PFGeoPoint				*reportingLocation;	// geo coordinates of user's institution for rapid map visualization


@end

NS_ASSUME_NONNULL_END
