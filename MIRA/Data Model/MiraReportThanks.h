//
//  MiraReportThanks.h
//  MIRA
//
//  Created by Bob Howard on 8/2/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <Parse/Parse.h>

NS_ASSUME_NONNULL_BEGIN

@interface MiraReportThanks : PFObject  <PFSubclassing>

+ (NSString *)parseClassName;

@property (nonatomic, strong) PFFileObject	*backgroundImage;	// contains the PDF data to view
@property (nonatomic, strong) NSString		*greetingText;		// article title for display
@property (nonatomic, strong) NSString		*bodyText;			// article title for display

@end

NS_ASSUME_NONNULL_END
