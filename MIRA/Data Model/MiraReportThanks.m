//
//  MiraReportThanks.m
//  MIRA
//
//  Created by Bob Howard on 8/2/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "MiraReportThanks.h"
#import <Parse/PFObject+Subclass.h>

@implementation MiraReportThanks

@dynamic backgroundImage;
@dynamic greetingText;
@dynamic bodyText;



+ (nonnull NSString *)parseClassName
{
	// allows Parse engine object serialization
	return NSStringFromClass([self class]);
}

@end
