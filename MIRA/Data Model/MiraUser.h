//
//  MiraUser.h
//  MIRA
//
//  Created by Bob Howard on 8/27/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <Parse/Parse.h>
#import "MiraInstitution.h"
#import "MiraProfessionalRole.h"

NS_ASSUME_NONNULL_BEGIN

@interface MiraUser : PFUser  <PFSubclassing>

+ (NSString *)parseClassName;

@property (nonatomic, strong) MiraInstitution		*institution;
@property (nonatomic, strong) MiraProfessionalRole	*profession;

@end

NS_ASSUME_NONNULL_END
