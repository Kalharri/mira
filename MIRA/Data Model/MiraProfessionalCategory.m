//
//  MiraProfessionalCategory.m
//  MIRA
//
//  Created by Bob Howard on 8/29/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "MiraProfessionalCategory.h"
#import <Parse/PFObject+Subclass.h>

@implementation MiraProfessionalCategory

@dynamic name;


+ (nonnull NSString *)parseClassName
{
	// allows Parse engine object serialization
	return NSStringFromClass([self class]);
}

@end
