//
//  StatsViewController.m
//  MIRA
//
//  Created by Bob Howard on 7/11/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "StatsViewController.h"
#import "InstitutionAnnotationView.h"
#import "InstitutionClusterView.h"

// data model
#import "MiraUser.h"
#import "MiraIncidentReport.h"
#import "MiraInstitution.h"


@interface StatsViewController ()

@property (weak, nonatomic) IBOutlet 	UIImageView			*fakeChartView;			// just an image, for now
@property (weak, nonatomic) IBOutlet 	MKMapView			*mapView;				// map displays geographic incident data

@property (nonatomic)		  			BOOL				chartShowing;			// if true, the charts are showing, otherwise the map

@end

@implementation StatsViewController


- (void)viewDidLoad
{
	[super viewDidLoad];
	
	// we initially view the chart, not the map
	self.chartShowing = true;
	
	[self configureMapView];
	
	// adjust map to show the user's facility's Neighborhood, if they've provided a location
	MiraUser *user = (MiraUser *)[PFUser currentUser];
	CLLocation *homeLocation = [[CLLocation alloc] initWithLatitude:user.institution.location.latitude longitude:user.institution.location.longitude];
	[self focusMapOnLocation:homeLocation WithRadius:13000.0];
/*
	// temp query gets all institutions, allows testing of annotation & cluster visualizations
	PFQuery *query = [MiraInstitution query];
	NSArray *nearObjects = [query findObjects];
	NSLog(@"found %ld institutions", nearObjects.count);
	
	for (MiraInstitution *institution in nearObjects)
	{
		CLLocationCoordinate2D location2D = CLLocationCoordinate2DMake(institution.location.latitude, institution.location.longitude);
		MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
		point.coordinate = location2D;
		point.title = institution.name;
		[self.mapView addAnnotation:point];
	}
*/
}


#pragma mark - MKMapViewDelegate protocol

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
	// get the map view's current region, compose a DB query for institutions in the region & perform the query
	MKCoordinateRegion localRegion = self.mapView.region;
	PFQuery *localQuery = [self institutionsInRegion:localRegion];
	NSArray *nearObjects = [localQuery findObjects];
	NSLog(@"%@", nearObjects);
	
	// iterate the institutions, creating map annotations for each
	for (MiraIncidentReport *report in nearObjects)
	{
		[report.reportingUser fetchIfNeeded];
		[report.reportingUser.institution fetchIfNeeded];

		PFGeoPoint *reportLocation = report.reportingUser.institution.location;
		CLLocationCoordinate2D location2D = CLLocationCoordinate2DMake(reportLocation.latitude, reportLocation.longitude);
		
		MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
		point.coordinate = location2D;
		[self.mapView addAnnotation:point];
	}
}


#pragma mark - user actions

- (IBAction)toggleChartMap:(id)sender
{
	NSUInteger animationOptions = 	UIViewAnimationOptionTransitionCrossDissolve |
									UIViewAnimationOptionShowHideTransitionViews;
	
	if (self.chartShowing)
	{
		// if the charts are currently showing, flip to the map
		[UIView transitionFromView:self.fakeChartView toView:self.mapView duration:(0.6) options:animationOptions completion:nil];
	}
	else
	{
		// otherwise, flip from map to charts
		[UIView transitionFromView:self.mapView toView:self.fakeChartView duration:(0.6) options:animationOptions completion:nil];
	}
	
	self.chartShowing = !self.chartShowing;
}


#pragma mark - helper methods

- (void)focusMapOnLocation:(CLLocation *)newCenter WithRadius:(CGFloat)newRadius
{
	MKCoordinateRegion newRegion = MKCoordinateRegionMakeWithDistance(newCenter.coordinate, newRadius, newRadius);
	
	[self.mapView setRegion:newRegion animated: true];
}

- (void)configureMapView
{
	// muted map style allows data to pop, with few distractions
	self.mapView.mapType = MKMapTypeMutedStandard;

	// tell MapKit to use our annotation & cluster views
    [self.mapView registerClass:[InstitutionAnnotationView class] forAnnotationViewWithReuseIdentifier:MKMapViewDefaultAnnotationViewReuseIdentifier];
    [self.mapView registerClass:[InstitutionClusterView class] forAnnotationViewWithReuseIdentifier:MKMapViewDefaultClusterAnnotationViewReuseIdentifier];
}

- (PFQuery *) institutionsInRegion:(MKCoordinateRegion)region
{
	PFQuery *query = [MiraIncidentReport query];
	
	PFGeoPoint *mapCenterPoint = [PFGeoPoint geoPointWithLatitude:region.center.latitude longitude:region.center.longitude];
	
	// MKMapSpan (iOS) stores degrees, while Parse location queries want radians, hence degToRadians
	[query whereKey:@"reportingLocation" nearGeoPoint:mapCenterPoint withinRadians: (M_PI * region.span.latitudeDelta) / 180.0];
	
	NSLog(@"user lat: %f, long: %f", region.center.latitude, region.center.longitude);
	NSLog(@"MKMapSpan deg: %f, withinRadians: %f", region.span.latitudeDelta, (M_PI * region.span.latitudeDelta) / 180.0);

	// Limit what could be a lot of points
	query.limit = 100;

	return query;
}

@end

/* Usefull reverse geocoding
	if (!user.institution.location)
	{
		NSString *zipString = [NSString stringWithFormat:@"%d", user.institution.zipcode.intValue];
		CLGeocoder *geocoder = [[CLGeocoder alloc] init];
	
		[geocoder geocodeAddressString:zipString
				 completionHandler:^(NSArray* placemarks, NSError* error)
		 		{
					if (placemarks && placemarks.count > 0)
					{
						CLPlacemark *topResult = [placemarks objectAtIndex:0];
						MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
			 
						CLLocation *location = [[CLLocation alloc] initWithLatitude:placemark.region.center.latitude longitude:placemark.region.center.longitude];
						
						user.institution.location = [PFGeoPoint geoPointWithLocation:location];
						[user saveInBackground];
					}
				}];
	}
*/
