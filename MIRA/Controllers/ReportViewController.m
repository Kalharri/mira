//
//  ReportViewController.m
//  MIRA
//
//  Created by Bob Howard on 7/11/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "ReportViewController.h"
#import "IncidentTableViewController.h"
#import "HarmPanelViewController.h"
#import "ReportView.h"
#import "ThanksViewController.h"

// data model
#import "MiraIncidentReport.h"
#import "MiraUser.h"
#import "MiraReportThanks.h"


@interface ReportViewController ()

@property (strong, nonatomic)				IncidentTableViewController *incidentTableController;
@property (strong, nonatomic)				HarmPanelViewController		*harmPanelController;

@property (weak, nonatomic)		IBOutlet	UIView		*incidentOverviewPanel;	// holds the chosen incident type's title and discussion text
@property (strong, nonatomic) 	IBOutlet	ReportView	*reportView;			// main view that manages switch between incident overview and table
@property (weak, nonatomic) 	IBOutlet	UISearchBar	*searchBar;				// guess?
@property (weak, nonatomic) 	IBOutlet	UILabel		*titleLabel;			// The incident overview's title
@property (weak, nonatomic) 	IBOutlet	UIButton	*submitButton;
@property (weak, nonatomic) 	IBOutlet	UIButton	*cancelButton;

- (IBAction)submitTapped:(id)sender;
- (IBAction)cancelTapped:(id)sender;


@end

@implementation ReportViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	// find the grandkid views & hook them to their properties				//***B	not sure this is needed for the harmpanel (IBOUTLET?)
	for (UIViewController *controller in self.childViewControllers)			//		perhaps when we need to access the panel's value to compose report record
	{
		if ([controller isKindOfClass:[IncidentTableViewController class]])
		{
			self.incidentTableController.reportController = self;
			self.incidentTableController = (IncidentTableViewController *)controller;
		}
		else if ([controller isKindOfClass:[HarmPanelViewController class]])
		{
			self.harmPanelController = (HarmPanelViewController *)controller;
		}
	}
	self.incidentTableController.reportController = self;
}

- (void) setIncidentTitle:(NSString *)incidentTitle
{
	self.titleLabel.text = incidentTitle;
}


#pragma mark - navigation

/* EXPERIMENTAL FAILURE
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([[segue identifier] isEqualToString:@"reportToThanksSegue"])
	{
		// get the destination controller
		ThanksViewController *thanksController = (ThanksViewController *)segue.destinationViewController;
		
		// retrieve the text and background image from the cloud
		[[MiraReportThanks query] getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error)
		 {
			 if (!error)
			 {
				 // load the views' content from the data object
				 MiraReportThanks *newThanks = (MiraReportThanks *)object;
				 
				 thanksController.dataObject = newThanks;
			 }
		 }];
	}
}
*/

#pragma mark - action handlers

- (void) incidentTypeSelected
{
	// cancel the current search, if any
	[self cancelSearch];
	
	// disable the searchBar. To start over, they press the Cancel button
	self.searchBar.userInteractionEnabled = false;
	self.searchBar.alpha = 0.65;
	self.searchBar.placeholder = @"";
	
	// enable the report submission buttons
	self.submitButton.enabled = true;
	self.cancelButton.enabled = true;

	// hide the incident type table, revealing the incident report
	[self.reportView sendListToBack:true];
}

- (void) cancelSearch
{
	// clear the search bar's text field
	self.searchBar.text = @"";
	
	// unfilter the list
	[self.incidentTableController cancelSearch];
	
	// dismiss the keyboard
	[self.searchBar resignFirstResponder];
}


#pragma mark - UISearchBarDelegate methods

- (void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)searchString
{
	// force all of the search bar's displayed input to lower case
	searchBar.text = searchString.lowercaseString;
	
	if (searchString.length > 0)
	{
		// filter the list according to the new search string
		[self.incidentTableController performSearch:searchString];
	}
	else
	{
		// the user's cleared the search bar, unfilter the list
		[self.incidentTableController cancelSearch];
	}
	
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
	// user tapped Done on the keyboard. cancel the search
	[self cancelSearch];
}

// - (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
// {
// 	NSLog(@"*** searchBarTextDidEndEditing ***");
// 	[searchBar resignFirstResponder];
// }

#pragma mark - User action handlers

- (void)searchBarCancelButtonClicked:(UISearchBar*)searchBar
{
	[self cancelSearch];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
	// show the incident selection table
	self.incidentTableController.tableView.hidden = false;
}


- (IBAction)submitTapped:(id)sender
{
	// ask the user to confirn the incident report submission and deal with Submit & Cancel actions
	UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Send the report?"
																   message:nil
															preferredStyle:UIAlertControllerStyleActionSheet];
	
	UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Submit" style:UIAlertActionStyleDefault
														  handler:^(UIAlertAction * action)
															{
																// construct the MiraIncidentReport and start saving it to the cloud
																MiraIncidentReport * report = [[MiraIncidentReport alloc] init];
																
																MiraUser *user = (MiraUser *)[PFUser currentUser];
																report.reportingUser = user;
																report.institution = user.institution;
																// report.reportingLocation = user.institution.location;
																report.patientHarm = [NSNumber numberWithInt:self.harmPanelController.harmLevel];
																
																[report saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error)
																{
																	NSLog (@"***saved incident report in cloud");
																}];
																
																// disable relevant controls, re-show the incident type table
																[self setupForNewReport];

																
									// ***B doesn't this belong in the completion block?							// segue to the acknoledgement page
																[self performSegueWithIdentifier:@"reportToThanksSegue" sender:self];
															}];
	[alert addAction:defaultAction];

	UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
														 handler:^(UIAlertAction * action)
								   							{
																[self setupForNewReport];
															}];
	[alert addAction:cancelAction];

	// display the action sheet modally
	[self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)cancelTapped:(id)sender
{
	[self setupForNewReport];
}


#pragma mark - helper methods

- (void) setupForNewReport
{
	// disable Submit & Cancel buttons till a new incident type is chosen by the user
	self.submitButton.enabled = false;
	self.cancelButton.enabled = false;
	
	// put the harmPanel back in it's pre-report state
	self.harmPanelController.on = false;
	
	// disable the searchBar. To start over, they need to press Cancel button
	self.searchBar.userInteractionEnabled = true;
	self.searchBar.alpha = 1.0;
	self.searchBar.placeholder = @"Search incident types";
	
	// hide the incident type list to expose the underlying controls
	[self.reportView sendListToBack:false];
	self.incidentTableController.tableView.hidden = false;
}

@end
