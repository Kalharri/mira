//
//  ReportViewController.h
//  MIRA
//
//  Created by Bob Howard on 7/11/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportViewController : UIViewController

@property (weak, nonatomic)	NSString	*incidentTitle;

- (void) incidentTypeSelected;
- (void) cancelSearch;

@end

