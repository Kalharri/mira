//
//  PageViewController.h
//  MIRA
//
//  Created by Bob Howard on 8/7/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PageViewController : UIViewController

@property NSUInteger pageIndex;
@property NSString *titleText;
// @property NSString *imageFile;

@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;

@end

NS_ASSUME_NONNULL_END
