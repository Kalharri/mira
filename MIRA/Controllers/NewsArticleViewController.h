//
//  NewsArticleViewController.h
//  MIRA
//
//  Created by Bob Howard on 7/14/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewsArticleViewController : UIViewController

@property (strong, nonatomic)	PFFileObject *articleFile;		// holds the PDF file for viewing

@end

NS_ASSUME_NONNULL_END
