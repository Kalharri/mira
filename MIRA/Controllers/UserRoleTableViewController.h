//
//  UserRoleTableViewController.h
//  MIRA
//
//  Created by Bob Howard on 7/24/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/PFQueryTableViewController.h>

// data model
#import "MiraProfessionalRole.h"
#import "MiraProfessionalCategory.h"


NS_ASSUME_NONNULL_BEGIN

@interface UserRoleTableViewController : PFQueryTableViewController

@property (nonatomic, strong) 	MiraProfessionalRole	*profession;

@end

NS_ASSUME_NONNULL_END
