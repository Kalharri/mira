//
//  IncidentTableViewController.m
//  MIRA
//
//  Created by Bob Howard on 7/24/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "IncidentTableViewController.h"
#import "IncidentTableViewCell.h"
#import "ReportViewController.h"


@interface IncidentTableViewController ()

@property (strong, nonatomic) NSMutableArray	*incidentTypes;			// the definitive list of all incidents
@property (strong, nonatomic) NSMutableArray	*filteredIncidentTypes;	// the list whose members get incrementally filtered as the user types
@property (strong, nonatomic) NSArray			*incidentCategories;	// self explanatory, no?


@end

@implementation IncidentTableViewController


- (void)viewDidLoad
{
	[super viewDidLoad];
	 
	self.incidentCategories = @[@"Medication",
							    @"Documents",
								@"Staff Behavior"
							    ];

	self.incidentTypes = [@[
						   @[@"Medication administered improperly",
							 @"Wrong medication dose or freqency",
							 @"Wrong medication given",
							 @"Medication not given",
							 @"Medication given to wrong patient",
							 @"Wrong medication formulation",
							 @"Wrong medication instructions",
							 @"Medication given was contraindicated",
							 @"Medication not properly stored",
							 @"Expired medication given"],

						   @[@"Order/request missing or unavailable",
							 @"Order/request delayed",
							 @"Order/request was for wrong patient",
							 @"Request returned wrong document",
							 @"Order/request unclear or incomplete"],
						   
						   @[@"Uncooperative or obstructive staff",
							 @"Rude/hostile/inappropriate staff",
							 @"Risky or dangerous staff",
							 @"Substance abuse by staff",
							 @"Harassment by staff",
						     @"Discrimination or prejudice by staff",
						     @"Verbal aggression or abuse by staff",
						     @"Physical aggression by staff",
						     @"Sexual assault by staff"]
						  ] mutableCopy];

	// initially, the definitive and filtered incident lists are identical
	self.filteredIncidentTypes = self.incidentTypes.copy;
}

- (void) performSearch:(NSString *)newSearchString
{
	NSMutableArray *filteredTypes = [NSMutableArray arrayWithCapacity:self.incidentTypes.count];
	
	NSArray *filtered;
	
	for (NSUInteger classIndex = 0; classIndex < self.incidentTypes.count; classIndex++)
	{
		// filter the incidentTypes array of arrays by those incidents containing searchString
		filtered = [self.incidentTypes[classIndex] filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id object, NSDictionary *bindings)
																				 {
																					 return [(NSString *)object localizedCaseInsensitiveContainsString:newSearchString];
																				 	}]];
		[filteredTypes addObject:filtered];
		
	}

	self.filteredIncidentTypes = filteredTypes.copy;

	// refresh the table to show the new subset of incidents
	[self.tableView reloadData];
}

- (void) cancelSearch
{
	// unfilter the list and refresh the table display
	self.filteredIncidentTypes = self.incidentTypes.copy;
	[self.tableView reloadData];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	// only one for now - //***B may use headers to group types of incidents, now that we're displaying them all
	return self.incidentCategories.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	// return the number of currently filtered incident types in the specified section
	return (((NSMutableArray *)self.filteredIncidentTypes[section])).count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"IncidentCell" forIndexPath:indexPath];
	
	 if (cell == nil)
	 {
		 cell = [[IncidentTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"IncidentCell"];
	 }
	
	NSMutableArray *thisSectionArray = ((NSMutableArray *) self.filteredIncidentTypes[indexPath.section]);

	if (indexPath.row < thisSectionArray.count)
	{
		// hand the cell the incident text - //***B in future, we might rate them and color the text or apply an led to the cell
		cell.textLabel.text = thisSectionArray[indexPath.row];
//		cell.textLabel.text = self.filteredIncidentTypes[indexPath.section][indexPath.row];
	}
	
	 return cell;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:@"IncidentTypeHeader"];
	
	if (headerView == nil)
	{
		[NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from storyboard"];
	}
	
	headerView.textLabel.textColor = [UIColor darkGrayColor];
	headerView.textLabel.text = self.incidentCategories[section];
	
	return headerView;
}

- (void)tableView:(UITableView *)otherTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	// set the text at the top of the ReportController's view and the explanatory text beneath.
	self.reportController.incidentTitle = self.filteredIncidentTypes[indexPath.section][indexPath.row];
//	self.reportController.incidentTitle = self.filteredIncidents[indexPath.row];
	
	// hide the incident table so the underlying text can be seen
	self.tableView.hidden = true;									// ***B Here's where we'll need to msg the reportController to do the
																	//		animated transition between table and incident description

	// reportController deals with switching table to incident overview views, canceling search, etc.
	[self.reportController incidentTypeSelected];
}

@end
