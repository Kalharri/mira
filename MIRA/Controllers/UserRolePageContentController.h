//
//  UserRolePageContentController.h
//  MIRA
//
//  Created by Bob Howard on 8/24/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "PageContentController.h"

#import "MiraProfessionalRole.h"


NS_ASSUME_NONNULL_BEGIN

@interface UserRolePageContentController : PageContentController

@property (strong, nonatomic)	MiraProfessionalRole *profession;

@end

NS_ASSUME_NONNULL_END
