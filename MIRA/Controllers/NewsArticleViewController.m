//
//  NewsArticleViewController.m
//  MIRA
//
//  Created by Bob Howard on 7/14/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "NewsArticleViewController.h"

#import <PDFKit/PDFKit.h>


@interface NewsArticleViewController ()

@property (weak, nonatomic) IBOutlet PDFView 	*pdfViewer;		// coukd just as easily be a web viewer

@end


@implementation NewsArticleViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

	// load the pdf document with data in the article file
	self.pdfViewer.document = [[PDFDocument alloc]initWithData:self.articleFile.getData];

	// configure the pdf viewer
	self.pdfViewer.displayMode = kPDFDisplaySinglePageContinuous;
	self.pdfViewer.autoScales = true;
	self.pdfViewer.displayDirection = kPDFDisplayDirectionVertical;
}

@end
