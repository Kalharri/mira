//
//  HarmPanelViewController.h
//  MIRA
//
//  Created by Bob Howard on 7/17/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HarmPanelViewController : UIViewController

@property (nonatomic)	int		harmLevel;		// current value (0-5) on the harm scale
@property (nonatomic)	BOOL	on;				// read or set the whole panel's state

@end


NS_ASSUME_NONNULL_END
