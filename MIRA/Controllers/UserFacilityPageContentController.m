//
//  UserFacilityPageContentController.m
//  MIRA
//
//  Created by Bob Howard on 8/24/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "UserFacilityPageContentController.h"
#import "UserFacilityTableViewController.h"


@interface UserFacilityPageContentController ()

@property (weak, nonatomic)		IBOutlet UITextField	*zipcodeTextField;
@property (strong, nonatomic)			 UserFacilityTableViewController	*facilityTableController;

@end


@implementation UserFacilityPageContentController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	for (UIViewController *controller in self.childViewControllers)
	{
		if ([controller isKindOfClass:[UserFacilityTableViewController class]])
		{
			self.facilityTableController = (UserFacilityTableViewController *)controller;
			break;
		}
	}
}

- (MiraInstitution *)institution
{
	return self.facilityTableController.institution;
}



- (IBAction)zipcodeChanged:(id)sender
{
	if (5 == ((UITextField *)sender).text.length)
	{
		[self.view endEditing:YES];
//		[super touchesBegan:touches withEvent:event];
	}
}

- (IBAction)zipcodeEntered:(id)sender
{
	UITextField *field = (UITextField *)sender;
	
	NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
	formatter.numberStyle = NSNumberFormatterDecimalStyle;
	self.facilityTableController.searchZipcode = [formatter numberFromString:field.text];
	
	[self.facilityTableController loadObjects];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	[self.view endEditing:YES];
	[super touchesBegan:touches withEvent:event];
}


#pragma mark - UITextFieldDelegate protocol

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	if (textField == self.zipcodeTextField)
	{
		[textField resignFirstResponder];
		textField.text = @"";
		return NO;
	}
	return YES;
}


@end
