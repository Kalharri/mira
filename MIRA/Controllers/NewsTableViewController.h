//
//  NewsTableViewController.h
//  MIRA
//
//  Created by Bob Howard on 7/14/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <Parse/PFQueryTableViewController.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewsTableViewController : PFQueryTableViewController

@end

NS_ASSUME_NONNULL_END
