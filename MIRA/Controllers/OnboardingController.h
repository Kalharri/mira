//
//  OnboardingController.h
//  MIRA
//
//  Created by Bob Howard on 8/8/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OnboardingController : UIViewController <UIPageViewControllerDataSource>

-(IBAction) nextPage:(id)sender;
-(IBAction) lastPage:(id)sender;


@end

NS_ASSUME_NONNULL_END
