//
//  OnboardingController.m
//  MIRA
//
//  Created by Bob Howard on 8/8/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "OnboardingController.h"
#import "PageContentController.h"
#import "UserFacilityPageContentController.h"
#import "UserRolePageContentController.h"

// data model
#import "MiraUser.h"
#import "MiraProfessionalRole.h"
#import "MiraInstitution.h"


@interface OnboardingController ()

@property (strong, nonatomic) 	MiraUser 	*candidate;

@property (weak, nonatomic) 	IBOutlet 	UIView 					*containerView;		// ***B this seems unused
@property (weak, nonatomic)		IBOutlet	UIButton				*nextButton;

@property (strong, nonatomic) 				UIPageViewController	*pageViewController;

@property (strong, nonatomic)				NSArray					*pageContentControllers;
@property (strong, nonatomic) 				NSArray 				*pageTitles;
@property (strong, nonatomic) 				NSArray 				*pageImages;

@end

@implementation OnboardingController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.pageTitles = @[@"Welcome to MIRA", @"MIRA is Unique", @"What's your role?", @"Your institution?"];
	
	
}


#pragma mar - user actions

-(IBAction) nextPage:(id)sender
{
	NSUInteger index = ((PageContentController*)self.pageViewController.viewControllers[0]).pageIndex;
	// 0-3
	if (index < self.pageTitles.count - 1)
	{
		UIViewController *destinationViewController = [self viewControllerAtIndex:++index];
		NSArray *viewControllers = @[destinationViewController];
		[self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
	}
	else
	{
		// when user facility screen's JoinUs! is tapped, assemble the candidate PFUser andmove to the create user screen
		UserRolePageContentController *roleController = (UserRolePageContentController *)self.pageContentControllers[2];
		self.candidate.profession = roleController.profession;
		UserFacilityPageContentController *institutionController = (UserFacilityPageContentController *)self.pageContentControllers[3];
		self.candidate.institution = institutionController.institution;
		NSLog(@"New user is a %@ at %@ in %@", self.candidate.profession.name, self.candidate.institution.name, self.candidate.institution.city);

		[self performSegueWithIdentifier:@"onboardingToCreateUserSegue" sender:self];
	}
}

-(IBAction) lastPage:(id)sender
{
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	NSString * segueName = segue.identifier;
	
	if ([segueName isEqualToString: @"OnboardingPageViewControllerEmbed"])
	{
		self.pageViewController = (UIPageViewController *) [segue destinationViewController];
		self.pageViewController.dataSource = self;

		UIViewController *startingViewController = [self viewControllerAtIndex:0];
		NSArray *viewControllers = @[startingViewController];
		[self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
		
		// Change the size of page view controller
		//	self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 30);
		
		[self addChildViewController:_pageViewController];
		[self.view addSubview:_pageViewController.view];
		[self.pageViewController didMoveToParentViewController:self];
	}
	else if ([segueName isEqualToString: @"onboardingToCreateUserSegue"])
	{
		// send the assembled user data to the user creator controller
	}
}


#pragma mark - PageViewController data source protocol

- (nullable UIViewController *)pageViewController:(nonnull UIPageViewController *)pageViewController viewControllerAfterViewController:(nonnull UIViewController *)viewController
{
	NSUInteger index = ((PageContentController*)viewController).pageIndex;
	
	if (index == NSNotFound)
	{
		return nil;
	}
	
	index++;
	
	if (index == [self.pageTitles count])
	{
		return nil;
	}
	
	return [self viewControllerAtIndex:index];
}

- (nullable UIViewController *)pageViewController:(nonnull UIPageViewController *)pageViewController viewControllerBeforeViewController:(nonnull UIViewController *)viewController
{
	NSUInteger index = ((PageContentController*) viewController).pageIndex;
	
	if ((index == 0) || (index == NSNotFound))
	{
		return nil;
	}
	
	index--;
	
	return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
	return [self.pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
	return ((PageContentController*)self.pageViewController.viewControllers[0]).pageIndex;
}


#pragma mark - helper methods

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index
{
	PageContentController *newController = nil;
	[self.nextButton setTitle:@"Next" forState:UIControlStateNormal];

	// lazy initiate the candidate User and the array of controllers for each page
	if (!self.candidate)
	{
		self.candidate = [[MiraUser alloc] init];
		
		self.pageContentControllers = [NSMutableArray arrayWithObjects: [self.storyboard instantiateViewControllerWithIdentifier:@"WelcomeScreen"],
									   									[self.storyboard instantiateViewControllerWithIdentifier:@"DifferentiatorsScreen"],
									   									[self.storyboard instantiateViewControllerWithIdentifier:@"UserRoleScreen"],
									   									[self.storyboard instantiateViewControllerWithIdentifier:@"UserFacilityScreen"],
									   nil];

	}
	
	newController = self.pageContentControllers[index];
	newController.titleText = self.pageTitles[index];
	newController.pageIndex = index;

	return newController;
}


@end
