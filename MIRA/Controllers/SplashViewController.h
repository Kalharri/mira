//
//  SplashViewController.h
//  MIRA
//
//  Created by Bob Howard on 7/12/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SplashViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
