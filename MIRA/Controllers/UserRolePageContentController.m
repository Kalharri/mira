//
//  UserRolePageContentController.m
//  MIRA
//
//  Created by Bob Howard on 8/24/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "UserRolePageContentController.h"
#import "UserRoleTableViewController.h"


@interface UserRolePageContentController ()

@property (strong, nonatomic)	UserRoleTableViewController	*roleTableController;

@end


@implementation UserRolePageContentController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	for (UIViewController *controller in self.childViewControllers)
	{
		if ([controller isKindOfClass:[UserRoleTableViewController class]])
		{
			self.roleTableController = (UserRoleTableViewController *)controller;
			break;
		}
	}
}

- (MiraProfessionalRole *)profession
{
	return self.roleTableController.profession;
}


@end
