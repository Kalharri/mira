//
//  UserRoleTableViewController.m
//  MIRA
//
//  Created by Bob Howard on 7/24/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "UserRoleTableViewController.h"


@interface UserRoleTableViewController ()

@property (nonatomic, retain) NSMutableDictionary *categories;
@property (nonatomic, retain) NSMutableDictionary *sectionToCategoryMap;


@end

@implementation UserRoleTableViewController


- (void)viewDidLoad
{
	[super viewDidLoad];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];

	if (self)
	{
		self.parseClassName = @"MiraProfessionalRole";
		self.textKey = @"name";
		self.pullToRefreshEnabled = NO;
		self.paginationEnabled = NO;
		self.objectsPerPage = 100;
		
		self.categories = [NSMutableDictionary dictionary];
		self.sectionToCategoryMap = [NSMutableDictionary dictionary];
	}
	return self;
}


#pragma mark - UITableViewDataSource protocol

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	    return self.categories.allKeys.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	// return the number of roles in the specified section
	NSString *categoryName = [self categoryForSection:section];
	NSArray *rowIndecesInSection = [self.categories objectForKey:categoryName];
	
	return rowIndecesInSection.count;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:@"TypeHeader"];
	
	if (headerView == nil)
	{
		[NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from storyboard"];
	}
	
	headerView.textLabel.text = [self categoryForSection:section];
	headerView.textLabel.font = [UIFont boldSystemFontOfSize:headerView.textLabel.font.pointSize + 1];

	return headerView;
}

#pragma mark - UITableViewDelegate protocol

- (void)tableView:(UITableView *)otherTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
	cell.accessoryType = UITableViewCellAccessoryCheckmark;
	self.profession = (MiraProfessionalRole *)[self objectAtIndexPath:indexPath];
	NSLog(@"PREFESSION FOR CANDIDTATE: %@, %@", self.profession.category.name, self.profession.name);
}

- (NSIndexPath *)tableView:(UITableView *)otherTableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
	cell.accessoryType = UITableViewCellAccessoryNone;
	
	return indexPath;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RoleTableViewCell" forIndexPath:indexPath];
	
	if (cell == nil)
	{
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"RoleTableViewCell"];
	}
	
	cell.textLabel.text = [object objectForKey:@"name"];

	return cell;
}


#pragma mark - PFQueryTableViewController overrides

// This method is called every time objects are loaded from Parse via the PFQuery
- (void)objectsDidLoad:(NSError *)error
{
	[super objectsDidLoad:error];
	
	NSLog(@"Count in objectsDidLoad: %lu", (unsigned long)[self.objects count]);
	
	// clear out the dictionaries
	[self.categories removeAllObjects];
	[self.sectionToCategoryMap removeAllObjects];
	
	NSInteger section = 0;
	NSInteger rowIndex = 0;
	
	for (PFObject *object in self.objects)
	{
		PFObject *obj = [object objectForKey:@"category"];
		[obj fetchIfNeeded];
		NSString *categoryName = [obj objectForKey:@"name"];
		
		NSMutableArray *objectsInSection = [self.categories objectForKey:categoryName];
		
		if (!objectsInSection)
		{
			// this is the first time we see this Category - create the array for member row indecies
			objectsInSection = [NSMutableArray array];
			
			// update the map with key as section number and data as the new array of row indecies
			[self.sectionToCategoryMap setObject:categoryName forKey:[NSNumber numberWithInteger:section++]];
		}
		
		// store this row's object in that array
		[objectsInSection addObject:[NSNumber numberWithInteger:rowIndex++]];
		[self.categories setObject:objectsInSection forKey:categoryName];
	}
	
	[self.tableView reloadData];
}

- (PFQuery *)queryForTable
{
	PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
	
	query.cachePolicy = kPFCachePolicyCacheThenNetwork;
	
	// If no objects are loaded in memory, we look to the cache first to fill the table
	// and then subsequently do a query against the network.
	if (self.objects.count == 0)
	{
		query.cachePolicy = kPFCachePolicyCacheThenNetwork; // ***B this if clause is quite suspicious
	}
	
	// Order by category					// ***B 2-key sort? primary: category, secondary: name
	[query orderByAscending:@"category"];
	[query addAscendingOrder:@"name"];

	return query;
}

- (PFObject *)objectAtIndexPath:(NSIndexPath *)indexPath
{
	// get category name to use as a key into the full set of retrieved roles
	NSString *categoryKey = [self categoryForSection:indexPath.section];
	
	// get the row indecies in the full set that correspond to this section's roles
	NSArray *rowIndecesInSection = [self.categories objectForKey:categoryKey];
	
	// return the object at that row in the section
	NSNumber *rowIndex = [rowIndecesInSection objectAtIndex:indexPath.row];

	return [self.objects objectAtIndex:[rowIndex intValue]];
}


#pragma mark - helper methods
	
- (NSString *)categoryForSection:(NSInteger)section
{
	// return the category nae for this section. Used both as a key and in labelling
	return [self.sectionToCategoryMap objectForKey:[NSNumber numberWithInteger:section]];
}


@end
