//
//  ThanksViewController.m
//  MIRA
//
//  Created by Bob Howard on 8/2/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "ThanksViewController.h"
#import <Parse/PFImageView.h>

// data model
#import "MiraReportThanks.h"


@interface ThanksViewController ()

@property (weak, nonatomic) IBOutlet PFImageView 		*backgroundImageView;	// displays bitmap, retrieved from DB
@property (weak, nonatomic) IBOutlet UINavigationItem	*greetingItem;			// the navBar item that vholds the title
@property (weak, nonatomic) IBOutlet UILabel			*bodyTextLabel;			// the actual thank you

- (IBAction)doneTapped:(id)sender;

@end


@implementation ThanksViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	// retrieve the text and background image from the cloud
	[[MiraReportThanks query] getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error)
	 {
		 if (!error)
		 {
			 // load the views' content from the data object
			 MiraReportThanks *newThanks = (MiraReportThanks *)object;
			 
			 self.backgroundImageView.file = newThanks.backgroundImage;
			 [self.backgroundImageView loadInBackground];
			 self.greetingItem.title = newThanks.greetingText;
			 self.bodyTextLabel.text = newThanks.bodyText;
		 }
	 }];
}


#pragma mark - user action handlers

- (IBAction)doneTapped:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:nil];
}
@end
