//
//  UserFacilityPageContentController.h
//  MIRA
//
//  Created by Bob Howard on 8/24/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "PageContentController.h"

#import "MiraInstitution.h"


NS_ASSUME_NONNULL_BEGIN

@interface UserFacilityPageContentController : PageContentController

@property (strong, nonatomic)	MiraInstitution *institution;

@end

NS_ASSUME_NONNULL_END
