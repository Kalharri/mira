//
//  CreateUserViewController.m
//  MIRA
//
//  Created by Bob Howard on 8/25/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "CreateUserViewController.h"
#import "CurveAnimation.h"
// #import </WinkLoadingView/W

@interface CreateUserViewController ()

@property (strong, nonatomic) CurveAnimation *loader;

@end

@implementation CreateUserViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.loader = [[CurveAnimation alloc] initWithView:self.view];
	[self.loader setup];
	
	[NSTimer scheduledTimerWithTimeInterval:2.0
									 target:self
								   selector:@selector(congratsAndSegue)
								   userInfo:nil
									repeats:NO];
}

- (void) congratsAndSegue
{
	// stop the loading animation
	[self.loader remove];
	
	// welcome the user
	
	// segue to the main interface
	[self performSegueWithIdentifier:@"welcomeToReportSegue" sender:self];

	
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
