//
//  StatsViewController.h
//  MIRA
//
//  Created by Bob Howard on 7/11/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>


@interface StatsViewController : UIViewController <CLLocationManagerDelegate, MKMapViewDelegate>

- (IBAction)toggleChartMap:(id)sender;			// received when the map/chart flip button is tapped

@end

