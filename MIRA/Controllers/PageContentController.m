//
//  TextImagePageController.m
//  MIRA
//
//  Created by Bob Howard on 8/7/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "PageContentController.h"

@interface PageContentController ()

@end

@implementation PageContentController

- (void)viewDidLoad
{
    [super viewDidLoad];

	self.titleLabel.text = self.titleText;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)nextTapped:(id)sender
{
}

@end
