//
//  SplashViewController.m
//  MIRA
//
//  Created by Bob Howard on 7/12/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "SplashViewController.h"
#import <Parse/Parse.h>

@interface SplashViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *backgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *logoType;
@property (weak, nonatomic) IBOutlet UILabel	 *taglineLabel;
// @property (weak, nonatomic) IBOutlet UIVisualEffectView *effectsView;

@end

@implementation SplashViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.backgroundView.alpha = 1.0;
	
	self.taglineLabel.alpha = 0.0;
	self.logoType.transform = CGAffineTransformMakeScale(0.01, 0.01);
	self.logoType.alpha = 1.0;

//	self.effectsView.alpha = 0.0;
	//	self.effectsView.effect = nil;
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	
	// Perform the splashscreen animation
	
	[UIView animateWithDuration:1.5
						  delay:0.5
						options:UIViewAnimationOptionCurveEaseIn
					 animations:^{
						 			self.logoType.transform = CGAffineTransformMakeScale(1.0, 1.0);
						 			self.taglineLabel.alpha = 1.0;
						 			self.backgroundView.alpha = 0.3;
								 }
					 completion:^(BOOL finished)
	 							 {
									 [UIView animateWithDuration: 0.6
														   delay: 1.0
														 options: UIViewAnimationOptionCurveEaseIn
													  animations:^{
																	  CGRect newFrame = self.taglineLabel.frame;
														  			  newFrame.origin = CGPointMake(newFrame.origin.x, self.view.frame.size.height);
														  			  self.taglineLabel.frame = newFrame;
																	  self.taglineLabel.alpha = 0.0;
														  
																	  newFrame = self.logoType.frame;
														  			  newFrame.origin = CGPointMake(newFrame.origin.x, -self.logoType.frame.size.height);
																	  self.logoType.frame = newFrame;
																	  self.logoType.alpha = 0.0;
																	  self.backgroundView.alpha = 0.0;
																	  self.view.alpha = 0.0;
																  }
													  completion:^(BOOL finished)
									  							  {
																	  BOOL member = 1;
																	  
																	  if (member) 			// ([PFUser currentUser].isAuthenticated)
																	  {
																		  // if user is a member, take them to the reporting screen
																		  [self performSegueWithIdentifier:@"splashToTabsSegue" sender:self];
																	  }
																	  else
																	  {
																		  // otherwise, take them to the onboarding sequence
																		  [self performSegueWithIdentifier:@"splashToOnboardingSegue" sender:self];
																	  }
																  }];
								 }];
}

#pragma mark - Navigation

/*
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
