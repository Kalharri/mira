//
//  UserFacilityTableViewController.m
//  MIRA
//
//  Created by Bob Howard on 8/24/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "UserFacilityTableViewController.h"

@interface UserFacilityTableViewController ()

@end

@implementation UserFacilityTableViewController

- (void)viewDidLoad
{
	self.searchZipcode = [NSNumber numberWithInt:0];
	
    [super viewDidLoad];
	
	
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
}

- (void) clearSelection
{
	NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
	UITableViewCell *selectedCell = [self.tableView cellForRowAtIndexPath:selectedIndexPath];
	selectedCell.accessoryType = UITableViewCellAccessoryNone;
	[selectedCell setSelected:false];
}


#pragma mark - Table view delegate protocol

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)otherTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
	cell.accessoryType = UITableViewCellAccessoryCheckmark;
	self.institution = (MiraInstitution *)[self objectAtIndexPath:indexPath];
	NSLog(@"ASSEMBLED CANDIDATE: %@ %@", self.institution.providerId, self.institution.name);
}

- (NSIndexPath *)tableView:(UITableView *)otherTableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
	cell.accessoryType = UITableViewCellAccessoryNone;
	
	return indexPath;
}


#pragma mark - PFQueryTableViewConrtroller overrides

- (PFQuery *)queryForTable
{
	PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
	[query whereKey:@"zipcode" equalTo:self.searchZipcode];

	// If objects not already loaded, check the cache. If no luck, query the network
	if (self.objects.count == 0)
	{
		query.cachePolicy = kPFCachePolicyCacheThenNetwork;
	}
	
	[query orderByAscending:@"name"];
	
	return query;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
