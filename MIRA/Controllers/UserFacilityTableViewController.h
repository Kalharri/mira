//
//  UserFacilityTableViewController.h
//  MIRA
//
//  Created by Bob Howard on 8/24/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/PFQueryTableViewController.h>

// data model
#import "MiraInstitution.h"


NS_ASSUME_NONNULL_BEGIN

@interface UserFacilityTableViewController : PFQueryTableViewController

@property (nonatomic, strong)	NSNumber		*searchZipcode;
@property (nonatomic, strong) 	MiraInstitution	*institution;

- (void) clearSelection;

@end

NS_ASSUME_NONNULL_END
