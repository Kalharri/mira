//
//  IncidentTableViewController.h
//  MIRA
//
//  Created by Bob Howard on 7/24/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ReportViewController;

NS_ASSUME_NONNULL_BEGIN

@interface IncidentTableViewController : UITableViewController

@property (strong, nonatomic)	ReportViewController	*reportController;

- (void) performSearch:(NSString *)newSearchString;
- (void) cancelSearch;

@end

NS_ASSUME_NONNULL_END
