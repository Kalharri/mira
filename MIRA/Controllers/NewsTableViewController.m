//
//  NewsTableViewController.m
//  MIRA
//
//  Created by Bob Howard on 7/14/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "NewsTableViewController.h"
#import "NewsArticleViewController.h"
#import "MeraNewsArticle.h"
#import "NewsCardTableViewCell.h"

#import <Parse/Parse.h>


@interface NewsTableViewController ()

@end

@implementation NewsTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.tableView.separatorColor = [UIColor clearColor];
}


#pragma mark - Table view delegate protocal

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (nullable PFTableViewCell *)tableView:(UITableView *)tableView
				  cellForRowAtIndexPath:(NSIndexPath *)indexPath
								 object:(nullable PFObject *)object
{
	NewsCardTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NewsCardCell" forIndexPath:indexPath];
	
	if (cell == nil)
	{
		cell = [[NewsCardTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"NewsCardCell"];
	}
	
	// hand the cell the article for display
	cell.article = (MeraNewsArticle *)object;;

	return (PFTableViewCell *)cell;
}


#pragma mark - Navigation

// prepare to navigate to the NewsArticleViewController scene
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([[segue identifier] isEqualToString:@"ReadArticleSegue"])
	{
		// get the selected row
		NSIndexPath *selPath = self.tableView.indexPathForSelectedRow;
		
		if (selPath != nil)
		{
			// get the file containing the PDF data to view
			PFFileObject *articleFile = [self objectAtIndexPath:selPath][@"bodyPDF"];
			
			// get a reference to the destination view controller
			NewsArticleViewController *articleController = [segue destinationViewController];
			
			// hand the PDF data to the next controller for display
			articleController.articleFile = articleFile;
		}
	}
}


@end
