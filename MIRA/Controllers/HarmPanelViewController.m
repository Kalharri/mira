//
//  HarmPanelViewController.m
//  MIRA
//
//  Created by Bob Howard on 7/17/19.
//  Copyright © 2019 Medical Incident Reporting Corporation. All rights reserved.
//

#import "HarmPanelViewController.h"
#import "ScaleButton.h"


@interface HarmPanelViewController ()

@property (weak, nonatomic) IBOutlet 	UISwitch						*harmSwitch;	// toggles entire panel ***B is this an excise tap?
@property (weak, nonatomic) IBOutlet 	UILabel							*minLabel;		// text for minimum scale value
@property (weak, nonatomic) IBOutlet 	UILabel							*maxLabel;		// text for maximum scale value
@property (strong, nonatomic) 			UISelectionFeedbackGenerator	*tapGenerator;	// taptic engine for generating clicks

@end


@implementation HarmPanelViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.harmLevel = 0;
	self.on = false;
	
	self.tapGenerator = nil;
}


#pragma mark - getter & setters

- (BOOL) on
{
	return self.harmSwitch.on;
}

- (void) setOn:(BOOL)on
{
	[self.harmSwitch setOn:on animated:true];
	[self harmSwitchToggled:self.harmSwitch];
}


#pragma mark - actions

- (IBAction)harmSwitchToggled:(id)sender
{
	// enable the level display both vidually and for interaction
	self.minLabel.textColor = self.harmSwitch.on ? [UIColor whiteColor] : [UIColor lightGrayColor];
	self.maxLabel.textColor = self.harmSwitch.on ? [UIColor whiteColor] : [UIColor lightGrayColor];
	[self enableLevelDisplay:self.harmSwitch.on];

	// if they're turning the switch off, drop the level to 0
	if (false == self.harmSwitch.on)
	{
		self.harmLevel = 0;
		[self updateLevelDisplay:self.harmLevel];
		self.tapGenerator = nil;
	}
	else
	{
		self.tapGenerator = [[UISelectionFeedbackGenerator alloc] init];
		[self.tapGenerator prepare];
		

	}
}

- (IBAction)harmLevelSelected:(id)sender
{
	if ([sender isKindOfClass:[ScaleButton class]])
	{
		// determine the newly-selected harm level
		ScaleButton *button = (ScaleButton *)sender;
		int newHarmLevel = button.titleLabel.text.intValue;
		
		if (newHarmLevel != self.harmLevel)
		{
			// update the entire level display to reflect the new harm level
			[self.tapGenerator selectionChanged];
			[self updateLevelDisplay:newHarmLevel];
			self.harmLevel = newHarmLevel;
		}
	}
}


#pragma mark - helper methods

- (void) updateLevelDisplay:(int) newLevel
{
	// iterate the controller's view collection, looking for ScaleButtons
	for (id element in self.view.subviews)
	{
		if ([element isKindOfClass:[ScaleButton class]])
		{
			// turn the button's selected property on  if its harm level <= newLevel, off if it's not
			ScaleButton *nextButton = (ScaleButton *)element;
			int buttonHarmLevel = nextButton.titleLabel.text.intValue;
			nextButton.selected = (buttonHarmLevel <= newLevel) ? true : false;
		}
	}
}

- (void) enableLevelDisplay:(BOOL) enable
{
	// iterate the controller's view collection and pass enable to all ScaleButtons
	for (id element in self.view.subviews)
	{
		if ([element isKindOfClass:[ScaleButton class]])
		{
			ScaleButton *nextButton = (ScaleButton *)element;
			nextButton.enabled = enable;
		}
	}
}


@end

/* ***B
 dispatch_async(dispatch_get_main_queue(),
 ^{
 	AudioServicesPlaySystemSound(1104);
 });
*/
